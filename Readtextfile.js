const fs = require('fs');
const path = require('path');
//Filepath basic 1
// console.log('Before File Reading')
fs.readFile('./Simple.txt', 'utf8', (Errorrec, data) => {
    if (Errorrec) {
        console.error(Errorrec)
        return
    }
    console.log('Basic 01', data)
});
// console.log('After File Reading');

//Filepath basic 2
const datasync = fs.readFileSync('./Simple.txt', 'utf8')
console.log('readfilesync Basic 02', datasync);


//Filepath basic 3
const filepath = path.join(__filename, '../people.txt')
const peopledata = fs.readFileSync(filepath, 'utf8')

const people = peopledata.split('\r\n');
const peoplearray = people[2].split('|');
const [firstName, lastName, Age] = people[0].split('|');
console.log(peopledata); // ยกมาจาก txt เลย
console.log(people); //ยกมาแต่จะ split ให้สวยงาม
console.log(peoplearray); //ออก array 0- 1 -2 ออก2

console.log('First Name', firstName);
console.log('last Name', lastName);
console.log('Age', Age);





const filepath_01 = path.join(__filename, '../people01.txt')
// const peopledata_01 = fs.readFileSync(filepath_01, 'utf8') //ทำไมต้องไปอ่านไฟล์ที่ไม่มีข้อมูล
const personname = {
    nameage: 25,
    namefirstname: 'Wari',
    namelastname: 'Aekrachanikul'
}

const newdata_01 = `${personname.namefirstname}|${personname.namelastname}|${personname.nameage}`
fs.writeFileSync(filepath_01, newdata_01, { encoding: 'utf8' })
