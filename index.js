const { Console } = require('console');
const readline = require('readline');
const readInterface = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function TestOne(a, b) {
    let z;
    z = a + b
    console.log('funtion ' + z);
}
function TestTwo(a, b) {
    let z;
    z = a + b
    console.log('funtion ' + z);
}

TestOne(1, 5);
TestTwo(1, 9);


readInterface.question('Enter your Score 0-100 : ', (ScoreInput) => {
    readInterface.close();
    const Score = Number(ScoreInput);
    //เงือนไข IF
    // if (ScoreInput < 0 || ScoreInput > 100) {
    //     console.log('You entered an invalid score.');
    //     return;
    // }
    // if (ScoreInput >= 80) {
    //     console.log('Excellent! You got A.');
    // } else if (ScoreInput >= 70) {
    //     console.log('Good job! You got A.');
    // } else if (ScoreInput >= 60) {
    //     console.log('Not bad! C is your grade.');
    // } else if (ScoreInput >= 50) {
    //     console.log('Becareful! D is your grade.');
    // } else {
    //     console.log('Try it harder, You have F.');
    // }
    //เงือนไข Switch Cash
    switch (true) {
        case (Score > 100):
            console.log('You entered an invalid score.');
            break;
        case (Score >= 80):
            console.log('Excellent! You got A ');
            break;
        case (Score >= 75):
            console.log('Excellent! You got B+');
            break;
        case (Score >= 70):
            console.log('Excellent! You got B ');
            break;
        case (Score >= 65):
            console.log('Excellent! You got C+ ');
            break;
        case (Score >= 60):
            console.log('Excellent! You got C ');
            break;
        case (Score >= 55):
            console.log('Excellent! You got D+ ');
            break;
        case (Score >= 50):
            console.log('Excellent! You got D ');
            break;
        case (Score >= 0):
            console.log('Excellent! You got F ');
            break;
        default:
            console.log('Please fill out the information again.');
    }

})